# tg1

Project tg1 downloads the morning/evening news episode of TG1 from Raiplay. Uses the cli command `yt-dpl` to download the file. Make sure to install [yt-dlp](https://github.com/yt-dlp/yt-dlp) first.
