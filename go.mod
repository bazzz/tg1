module gitlab.com/bazzz/tg1

go 1.20

require (
	gitlab.com/bazzz/cfg v0.0.0-20221031102215-280e27138222
	gitlab.com/bazzz/dates v0.0.0-20220222123917-6929d3f6e4fa
	gitlab.com/bazzz/file v0.0.0-20220303080129-7b06d0a2508e
	gitlab.com/bazzz/logging v0.0.0-20230919074736-179b967e340a
	gitlab.com/bazzz/nfo v0.0.0-20230918085924-b1690b541060
	gitlab.com/bazzz/textparse v0.0.0-20230221085845-cd4301ef17d1
)
