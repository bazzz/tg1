package tg1

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os/exec"
	"path/filepath"
	"strconv"
	"time"

	"gitlab.com/bazzz/dates"
	"gitlab.com/bazzz/file"
	"gitlab.com/bazzz/nfo"
	"gitlab.com/bazzz/textparse"
)

const baseURL = "https://www.raiplay.it"

type Tg1 struct {
	ytdlpPath  string
	outputPath string
}

// New returns a new Tg1 client.
func New(ytdlpPath string, outputPath string) (*Tg1, error) {
	if !file.Exists(ytdlpPath) {
		return nil, errors.New("ERROR: ytdlpPath does not exist")
	}
	if !file.Exists(outputPath) {
		return nil, errors.New("ERROR: outputpath does not exist")
	}
	tg1 := Tg1{
		ytdlpPath:  ytdlpPath,
		outputPath: outputPath,
	}
	return &tg1, nil
}

// DownloadToday is a shorthand for Download that downloads today's morning episode.
func (c *Tg1) DownloadToday() error {
	return c.Download(time.Now(), true)
}

// DownloadYesterday is a shorthand for Download that downloads yesterday's evening episode.
func (c *Tg1) DownloadYesterday() error {
	return c.Download(time.Now().AddDate(0, 0, -1), false)
}

// Download gets the Tg1 morning episode of the specified date.
func (c *Tg1) Download(date time.Time, morning bool) error {
	filename := "Tg1 S" + date.Format("2006E0102")[2:]
	destinationPath := filepath.Join(c.outputPath, filename+".%(ext)s")
	response, err := http.Get(baseURL + "/palinsesto/app/rai-1/" + date.Format("02-01-2006") + ".json")
	if err != nil {
		return fmt.Errorf("ERROR: could not download tv guide: %w", err)
	}
	defer response.Body.Close()
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return fmt.Errorf("ERROR: could not read response data: %w", err)
	}
	episodeTime := "20:00"
	if morning {
		episodeTime = "08:00"
	}
	text := textparse.Take(string(data), `"hour" : "`+episodeTime+`"`, `, "event_weblink"`)
	url := textparse.Take(text, `"weblink" : "`, `"`)
	if url == "" {
		return errors.New("ERROR: could not find url for Tg1 morning episode")
	}
	output, err := excute(c.ytdlpPath, "--write-thumbnail", "-o", destinationPath, baseURL+url)
	if err != nil {
		return fmt.Errorf("ERROR: could not start yt-dlp: %w %v", err, output)
	}
	if err := c.fixThumbnail(filename); err != nil {
		log.Println("WARNING:", err)
	}
	if err := c.writeNFO(filename, date); err != nil {
		log.Println("WARNING:", err)
	}
	log.Println("Downloaded", filename)
	return nil
}

func (c *Tg1) fixThumbnail(filename string) error {
	path := filepath.Join(c.outputPath, filename+".jpg")
	if !file.Exists(path) {
		return fmt.Errorf("could not fix thumbnail: %w", errors.New("does not exist: "+path))
	}
	newPath := filepath.Join(c.outputPath, filename+"-thumb.jpg")
	file.Move(path, newPath)
	return nil
}

func (c *Tg1) writeNFO(filename string, date time.Time) error {
	path := filepath.Join(c.outputPath, filename+".nfo")
	number, _ := strconv.Atoi(date.Format("0102"))
	aired, _ := dates.New(date.Year(), int(date.Month()), date.Day())
	episode := nfo.Episode{
		Title:  "Tg1 " + date.Format("02-01-2006"),
		Season: date.Year(),
		Number: number,
		Aired:  &aired,
		Plot:   date.Format("02-01-2006"),
	}
	if err := episode.WriteNFO(path); err != nil {
		return fmt.Errorf("could not write nfo: %w", err)
	}
	return nil
}

func excute(args ...string) (string, error) {
	command := exec.Command(args[0], args[1:]...)
	output, err := command.CombinedOutput()
	if err != nil {
		return "", err
	}
	return string(output), nil
}
