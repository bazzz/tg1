package main

import (
	"log"

	"gitlab.com/bazzz/cfg"
	"gitlab.com/bazzz/logging"
	"gitlab.com/bazzz/tg1"
)

func main() {
	logging.Connect()
	log.SetOutput(logging.Default())
	config, err := cfg.New()
	if err != nil {
		log.Fatal(err)
	}
	ytdlp := config.Get("Yt-dlp")
	outputPath := config.Get("OutputPath")
	client, err := tg1.New(ytdlp, outputPath)
	if err != nil {
		log.Fatal(err)
	}
	if err := client.DownloadYesterday(); err != nil {
		log.Fatal(err)
	}
}
